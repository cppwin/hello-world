#include <string>
#include <iomanip>
#include <iostream>
#include <winrt/base.h>
#include <winrt/Windows.Foundation.h>
#include <winrt/Windows.Foundation.Collections.h>
#include <winrt/Windows.ApplicationModel.h>
#include <winrt/Windows.ApplicationModel.Activation.h>
#include <winrt/Windows.UI.h>
#include <winrt/Windows.UI.Xaml.h>
#include <winrt/Windows.UI.Xaml.Controls.h>
#include <winrt/Windows.UI.Xaml.Media.h>


using namespace std;

using namespace winrt;

using namespace Windows;
using namespace Windows::ApplicationModel::Activation; 
using namespace Windows::Foundation;
using namespace Windows::Foundation::Collections;
using namespace Windows::Storage;
using namespace Windows::Storage::Pickers;
using namespace Windows::UI;
using namespace Windows::UI::Xaml;
using namespace Windows::UI::Xaml::Controls;
using namespace Windows::UI::Xaml::Media;


// Example code from CppCon 2016.
// "Embracing Standard C++ for the Windows Runtime."

struct App : ApplicationT<App> 
{
    void OnLaunched(LaunchActivatedEventArgs const &)
    {
        TextBlock block; 
        block.FontFamily(FontFamily(L"Segoe UI Semibold"));
        block.FontSize(140.0); 
        block.Foreground(SolidColorBrush(Colors::HotPink()));
        block.VerticalAlignment(VerticalAlignment::Center); 
        block.TextAlignment(TextAlignment::Center);
        block.Text(L"Hello CppWinRT!");
        Window window = Window::Current();
        window.Content(block); window.Activate();
    }
};

int __stdcall wWinMain(HINSTANCE, HINSTANCE, PWSTR, int)
{
    Application::Start([](auto &&) { make<App>(); });
}
